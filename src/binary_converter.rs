pub fn convert_to_binary(mut num_to_convert: u64) -> String {
    // Purpose:    Converts a decimal number to a binary representation
    // Parameters: A decimal number, as an integer
    // User Input: No
    // Prints:     Nothing
    // Returns:    Result as a std::string of 1s and 0s
    // Modifies:   Nothing
    // Calls:      Basic python only
    // Tests:      ./unit_tests/*
    // Status:     Done!
    if num_to_convert == 0 {
        return String::from("0");
    }
    let mut result = String::new();
    while num_to_convert > 0 {
        if num_to_convert % 2 == 0 {
            result.insert(0, '0')
        } else {
            result.insert(0, '1')
        }
        num_to_convert /= 2;
    }
    result
}

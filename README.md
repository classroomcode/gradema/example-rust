# Example Rust

This Rust example follows a few conventions

* https://doc.rust-lang.org/rust-by-example/cargo/test.html
  * This means that the `tests` directory contains ONLY unit tests. This makes this project follow Rust conventions first, and any Gradema conventions second
  * If you don't want to use the `tests` directory, see this: https://users.rust-lang.org/t/how-can-i-have-a-custom-test-dir/81298
* `autograder` contains Python code and goal/input files like all Gradema projects

## Rust Installation

https://doc.rust-lang.org/cargo/getting-started/installation.html

## TODO Protect files

* `Cargo.toml`
* `tests`
* `grade.sh`
* `setup_venv.sh`
* `requirements.txt`
* `autograder`

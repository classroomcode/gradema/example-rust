@echo off
setlocal

:: Determine script directory
set "BASEDIR=%~dp0"

:: Change directory to script directory
cd /d "%BASEDIR%" || exit /b 1

:: Check if .venv directory exists
if not exist ".venv" (
    call setup_venv.bat || exit /b 1
)

:: Activate virtual environment
call .venv\Scripts\activate.bat
if errorlevel 1 (
    echo Failed to activate virtual environment
    exit /b 1
)

:: Run autograder
python -m autograder %*
if errorlevel 1 (
    echo Failed to run autograder
    exit /b 1
)

exit /b 0
